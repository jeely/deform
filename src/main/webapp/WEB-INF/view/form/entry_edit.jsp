<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>  
<meta charset="utf-8">
  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<meta name="keywords" content="模板工单列表">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/lib/easyui/1.5.3/themes/metro/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/lib/easyui/1.5.3/themes/icon.css"> 

    <link href="${pageContext.request.contextPath}/res/css/bootstrap/css/bootstrap.css?2023" rel="stylesheet" type="text/css" />
 
    <link href="${pageContext.request.contextPath}/res/css/site.css?2023" rel="stylesheet" type="text/css" />
 	  <script type="text/javascript" src="${pageContext.request.contextPath}/lib/jquery/1.9.1/jquery.min.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/lib/jquery/jquery.serializejson.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/lib/easyui/1.5.3/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/lib/easyui/1.5.3/locale/easyui-lang-zh_CN.js"></script>  
 	  
 	    <link href="${pageContext.request.contextPath}/res/js/datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
 <script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/res/js/datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/res/js/datepicker/dist/locales/bootstrap-datepicker.zh-CN.min.js"></script>	  
 	  
	<script type="text/javascript">
	$.fn.serializeObject = function () {
	    var o = {};
	    //var a = this.serializeArray();
	    var obj = this.serializeJSON();
	     
	   var $checks = $('input[type=checkbox]' , this);
	   $.each($checks,function(){
	    	var name = this.name; 
	      
	        var c_vs = obj[this.name];
	        if(!(c_vs instanceof Array) ){
	        	obj[this.name] = [];
	        }
	        var checked = this.checked; 
	        if(checked ) {
	        	obj[this.name].push(this.value);
	        }
	         
	    });
	    
	    var $radio = $('input[type=radio]',this);
	    $.each($radio,function(){
	    	 
	        if(!obj.hasOwnProperty(this.name)){
	            obj[this.name] = '';
	        }
	    });
	    return obj;
	};
	
	
	</script>
 <title>工单填写</title>

  <style>
html, body {
	height: 100%;
	margin: 0;
	padding: 0;
	font: small/1.5 "宋体", serif; 
}
 
</style>

</head>
<body>
    
	 <div style="width: 100%;height:100%;padding: 10px;">
	  
		 	<form id="myFormDesign" name="myFormDesign"  >
		 		<input type="hidden" id="form_id" name="form_id" value="${form_id}">
		 		<input type="hidden" id="id" name="id" value="${id}">
		 	 	  ${edit }
		 	 	
		 	</form>  
		 	
		 	
 			<div style="margin-bottom:20px;width: 100%;text-align: center;color: highlight;background-color: gray;">  
            	  <a href="#" class="easyui-linkbutton" id="close" data-options="iconCls:'icon-close'" style="width:80px">关闭</a>
        		  <a href="#" class="easyui-linkbutton" id="save" data-options="iconCls:'icon-save'" style="width:80px">保存</a>
        	
        	</div>  
		 
<script type="text/javascript">
    
    
	$('#save').bind('click', function(){
		
		 
	    // var value = JSON.stringify($('#myFormDesign').serializeJSON());
	     
	     var obj = $("#myFormDesign").serializeObject();
	       
	   
	     var value = JSON.stringify(obj);
	      
		var form_id =  $('#form_id').val();
		var id = $('#id').val();
	   
		
		var data = {"value":value , "form_id" : form_id ,  id:id};
		 
		
		 $.ajax({  
 	         url: './saveOrUpdateEntry' ,  
 	         type: 'POST',  
 	         data: data,   
 	         success: function (ret) {   
 	       	  	if(ret && ret.message) {
 	       	  		$.messager.show({
 	       	  			title:'提示',
 	       	  			msg: ret.message ,
 	       	  			timeout:3000,
 	       	  			showType:'slide'
 	       	  		});
 	       	  		
 	       	  		
 				
 				} else if(ret && ret.status == 0){ 
 					//数据
 					$.messager.show({
 	       	  			title:'提示',
 	       	  			msg: '添加成功' ,
 	       	  			timeout:3000,
 	       	  			showType:'slide'
 	       	  		}); 
 					setTimeout(function(){
 	       	  			window.close();
 	       	  		} , 1000)
 				}
 	       	   
 	         },  
 	         error: function (returndata) {  
 	            // alert('fail:' + returndata);   
 	             $.messager.progress('close');
 	         } 
 	    });   
		
		 
   		
	        
	 });
 	
 	
   	 	
   	 	 
</script>
 
  
</body>
</html>